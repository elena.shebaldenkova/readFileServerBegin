package main


import (
	model "gitlab.com/elena.shebaldenkova/readFileClientBegin/model"
	"log"
	"net"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"time"
	"fmt"
	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"path/filepath"
	"io"
	"github.com/spf13/viper"
)

type server struct{}

/*
const (
	Port string = ":8080"
 	DBName string = "my.db"
	BucketName string = "phonebook"
)
*/




func main() {
	viper.SetConfigName("config")     // no need to include file extension
	path,err:= filepath.Abs("../config/")
	fmt.Println("Config path:", path)
	if err != nil {
		fmt.Println("Config file not found...")
	}
	viper.AddConfigPath(path)
	// set the path of your config file
	error := viper.ReadInConfig()
	if error != nil {
		fmt.Println("Config file not found...")
	} else {
		serverPort := viper.GetString("server.port")
		runGRPC(serverPort)
	}
}
	//старт сервера
func runGRPC(Port string) {
	fmt.Println("Server start on:", Port)
	lis, err := net.Listen("tcp", Port)
	if err != nil {
		log.Fatalf("net.Listen() Error: %v", err)
		fmt.Println("Server start err:", err)
	}

	serv := grpc.NewServer()

	model.RegisterRecServiceServer(serv, &server{})
	reflection.Register(serv)
	fmt.Println("Server start Register:", Port)
	if err := serv.Serve(lis); err != nil {
		log.Fatalf("serv.Serve() Error: %v", err)
		fmt.Println("Server1 start err:", err)
	}
	fmt.Println("Server start end:", Port)
}


func (s *server) SendRecordsFromFileToBolt (stream model.RecService_SendRecordsFromFileToBoltServer) error {
	startTime := time.Now()
	err := DownloadToDB(stream)
	endTime := time.Now()
	fmt.Println("Diff =", endTime.Sub(startTime))

	return err
}

func DownloadToDB(stream model.RecService_SendRecordsFromFileToBoltServer) error {
	//параметры из конфигфайла
	DBName := viper.GetString("server.dbname")
	BucketName := viper.GetString("server.bucketname")
	//путь к бд
	path, err := filepath.Abs(DBName)
	if err != nil {
		log.Fatalf("did not find the path %v. Error: %v", path, err)
	}
	//открытие бд
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatalf("did not open DB %v. Error: %v", DBName, err)
	}
	defer db.Close()
	//открываем транзакцию
	startTimeBegin := time.Now()
	tx, err := db.Begin(true)
	if err != nil {
		log.Fatalf("db.Begin(true) Error: %v", err)
	}

	//счетчик  записей
	counter := 0
	for {
		bucket, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			log.Fatalf("tx.CreateBucketIfNotExists() Error: %v", err)
		}

		request, err := stream.Recv()
		if err == io.EOF {
			if err := tx.Commit(); err != nil {
				log.Fatalf("tx.Commit() Error: %v", err)
			}
			return stream.SendAndClose(&model.Response{
				Message: fmt.Sprintf("%s %d", "Read records:", counter),
			})
		}


		if err != nil {
			log.Fatalf("stream.Recv() Error: %v", err)
		}
		if contact, err := proto.Marshal(&model.Record{}); err == nil {
			if err := bucket.Put([]byte(request.Phonenumber), contact); err != nil {
				log.Fatalf("bucket.Put() Error: %v", err)
			}
		} else {
			log.Fatalf("proto.Marshal() Error: %v", err)
		}
		counter++
		if counter%10000000 == 0 {
			//коммит транзакции по достижении кол-ва записей в пачке
			if err := tx.Commit(); err != nil {
				log.Fatalf("tx.Commit() Error: %v", err)
			}
			endTimeBegin := time.Now()
			fmt.Println("written records:", counter, "Time to load 10000000 =", endTimeBegin.Sub(startTimeBegin))
			//открытие следующей транзакции
			tx, err = db.Begin(true)
			if err != nil {
				log.Fatalf("db.Begin(true) Error: %v", err)
			}
		}
	}
	return err
}

