Установка:
===
###### 1. Скачать данный проект командой go get gitlab.com/elena.shebaldenkova/readFileServerBegin.git  - серверная часть

###### 2. Установить зависимоти protobuf, boltDB, grpc (при необходимости):

go get github.com/golang/protobuf/protoc-gen-go
go get github.com/boltdb/bolt/...
go get -u google.golang.org/grpc

###### 3. Установка компилятора protobuf (при его отсутствии):

* перейдите по ссылке <https://github.com/google/protobuf/releases>
* скачайте zip архив protoc-3.5.0-win32.zip
* распакуйте и положите эту папку в $GOPATH/bin
* в терминале перейдите в $GOPATH/bin/protoc-3.5.0-win32/bin
* из терминала запустите команду: `protoc -I=C:\GoPath\src -I=C:\GoPath\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --grpc-gateway_out=logtostderr=true:C:\GoPath\src --go_out=plugins=grpc:C:\GoPath\src C:\GoPath\src\readFileClientBegin\model\phonebook.proto`

===

###### Для запуска GRPC сервера - из папки приложения запустить в терминале комманду `go run main.go`


Важно!
вместе с папкой проекта надо скачать отдельный пакет Сonfig. Там содержится настройки и для клиента и для сервера
папка содержит вынесенную отдельно конфигурация для проектов readFileClientBegin и readFileServerBegin
скачать всю папку, разархивировать и положить в ту же папку, где и вышеназванные проекты.
До запуска сервера и клиента - прописать в конфигфайле нужные пути, названия, сервер

